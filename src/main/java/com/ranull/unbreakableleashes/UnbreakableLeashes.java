package com.ranull.unbreakableleashes;

import com.ranull.unbreakableleashes.command.UnbreakableLeashesCommand;
import com.ranull.unbreakableleashes.integration.WorldGuard;
import com.ranull.unbreakableleashes.listener.EntityPortalListener;
import com.ranull.unbreakableleashes.listener.EntityUnleashListener;
import com.ranull.unbreakableleashes.listener.PlayerTeleportListener;
import com.ranull.unbreakableleashes.manager.LeashManager;
import org.bstats.bukkit.MetricsLite;
import org.bukkit.command.PluginCommand;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.java.JavaPlugin;

public class UnbreakableLeashes extends JavaPlugin {
    private WorldGuard worldGuard;
    private LeashManager leashManager;

    @Override
    public void onLoad() {
        if (getServer().getPluginManager().getPlugin("WorldGuard") != null) {
            try {
                Class.forName("com.sk89q.worldguard.WorldGuard", false, getClass().getClassLoader());
                Class.forName("com.sk89q.worldguard.protection.flags.registry.FlagConflictException",
                        false, getClass().getClassLoader());

                worldGuard = new WorldGuard(this);
            } catch (ClassNotFoundException ignored) {
            }
        }
    }

    @Override
    public void onEnable() {
        leashManager = new LeashManager(this);

        saveDefaultConfig();
        registerMetrics();
        registerCommands();
        registerListeners();
    }

    @Override
    public void onDisable() {
        unregisterListeners();
    }

    private void registerMetrics() {
        new MetricsLite(this, 13221);
    }

    private void registerCommands() {
        PluginCommand unbreakableLeashesPluginCommand = getCommand("unbreakableleashes");

        if (unbreakableLeashesPluginCommand != null) {
            UnbreakableLeashesCommand unbreakableLeashesCommand = new UnbreakableLeashesCommand(this);

            unbreakableLeashesPluginCommand.setExecutor(unbreakableLeashesCommand);
            unbreakableLeashesPluginCommand.setTabCompleter(unbreakableLeashesCommand);
        }
    }

    private void registerListeners() {
        getServer().getPluginManager().registerEvents(new PlayerTeleportListener(this), this);
        getServer().getPluginManager().registerEvents(new EntityUnleashListener(this), this);
        getServer().getPluginManager().registerEvents(new EntityPortalListener(this), this);
    }

    private void unregisterListeners() {
        HandlerList.unregisterAll(this);
    }

    public LeashManager getLeashManager() {
        return leashManager;
    }

    public WorldGuard getWorldGuard() {
        return worldGuard;
    }
}
